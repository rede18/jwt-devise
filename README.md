# JwtDevise
A plugin that enables Devise-powered applications to accept and manage JWT.

## Usage
This plugin allows your application to issue JWT tokens for users, 
as well as enable resources to be consumed with tokens. 

The default settings are: JWT lives no longer than 15 minutes, 6 months for refresh tokens. The leeway 
for expired tokens are 2 weeks.

To get a JWT for a user:

POST /jwt/sign_in.json

Body:
{
	"user": 
	{
		"email": "{YOUR EMAIL}",
		"password": "{YOUR PASSWORD}"
	}
}

This will respond with a JSON containing two keys: "access_token" and "refresh_token".

To refresh a token for a user after 15 minutes and before 2 weeks:

POST /jwt/refresh.json

Header:
Authorization: Bearer + {YOUR EXPIRED JWT}

Body:
{
	"refresh_token":
	{
		"token": "{YOUR REFRESH TOKEN LIES HERE}"
	}
}

Remember that refresh tokens can be invalidated before its original expiration of up to 6 months.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'jwt_devise'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install jwt_devise
```

Copy the migrations:
```bash
$ rails jwt_devise_engine:install:migrations
```

Don't forget to run the migrations:
```bash
$ rails db:migrate
```

Generate a private key to be used with RS256 algorithm:
```bash
$ OpenSSL::PKey::RSA.generate(2048).export
```

Store securely as a key in config/secrets.yml:
```bash
jwt_secret: "YOUR KEY HERE"
```

Adds jwt_authenticatable to your Devise model:
```bash
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :jwt_authenticatable
```

Mount engine to your project's config/routes.rb:
```bash
mount JwtDevise::Engine, at: '/jwt'
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
