JwtDevise.user_class = 'User'
JwtDevise.audience = 'TEST'
JwtDevise.issuer = 'TEST'
JwtDevise.expiration = 15.minutes.to_s
JwtDevise.secret = Rails.application.secrets.jwt_secret
JwtDevise.algorithm = 'RS256'
JwtDevise.leeway = 2.weeks
JwtDevise.refresh_expiration = 6.months
JwtDevise.possible_credentials = [:email, :cpf]
JwtDevise.maximum_active_devices = 10
JwtDevise.user_scope = nil
JwtDevise.scope_error = nil
JwtDevise.notify_scope_error = false

Devise.setup do |config|
  # Adding JWT strategy
  config.warden do |manager|
    # Registering your new Strategy
    manager.strategies.add(:jwt, Devise::Strategies::JsonWebToken)

    # Adding the new JWT Strategy to the top of Warden's list,
    # Scoped by what Devise would scope (typically :user)
    manager.default_strategies(scope: :user).unshift :jwt
  end
end