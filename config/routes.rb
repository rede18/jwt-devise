JwtDevise::Engine.routes.draw do
  post :sign_in, to: 'sessions#create'
  post :refresh, to: 'refresh_token#create'
  post :sign_out, to: 'refresh_token#destroy'
end
