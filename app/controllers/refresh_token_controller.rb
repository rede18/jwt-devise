class RefreshTokenController < ApplicationController

  def create
    current_user = find_user
    if current_user

      rt, refresh_token_id = refresh_token_params[:token].split('.')

      refresh_token = find_refresh_token(current_user, refresh_token_id)

      if refresh_token
        if check_validity(refresh_token.token, rt)
          if has_access_token_expired?
            # Retrieves a new access token if it has already expired
            token = refresh_token.user.encode_my_jwt
          else
            # Retrieves the same access_token if it is still valid
            strategy, token = JwtDevise::Wrapper.split(request.headers)
          end
          return render status: :ok, json: {access_token: token}
        end
      end


    end

    render status: :unauthorized, json: {error: I18n.t('errors.token_invalid')}
  end

  def destroy
    current_user = find_user(false)
    if current_user
      rt, refresh_token_id = refresh_token_params[:token].split('.')

      refresh_token = find_refresh_token(current_user, refresh_token_id)

        if check_validity(refresh_token.token, rt)
          if refresh_token.update(expires_at: Time.now)
            return render status: :ok, json: {success: I18n.t('successes.token_expired')}
          end
        end

    end

    render status: :unauthorized, json: {error: I18n.t('errors.token_expired')}
  end

  private

  def refresh_token_params
    params.require(:refresh_token).permit(:token)
  end

  def has_access_token_expired?
    JwtDevise::Wrapper.claims(request.headers).nil?
  end

  def find_user(with_leeway = true)
    options = with_leeway ? { :leeway => JwtDevise.leeway } : {}
    claims = JwtDevise::Wrapper.claims(request.headers, options)
    return JwtDevise.user_class.find_by_id claims['sub'] unless claims.nil?
    nil
  end

  def check_validity(stored_token, token)
    return true if BCrypt::Password.new(stored_token) == token
    false
  end

  def find_refresh_token(current_user, id)
    if id and /\A\d+\z/.match(id).present?
      current_user.refresh_tokens.active.find(id)
    else
      nil
    end
  rescue ActiveRecord::RecordNotFound
    nil
  end
end

