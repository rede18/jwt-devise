class SessionsController < ApplicationController
  def create
    if params && user_params && has_valid_params?
      if unscoped_user
        if wrong_scope
          error_message = scope_error || I18n.t('errors.wrong_scope')
          return render status: :forbidden, json: {error: error_message} if notify_scope_error
        elsif JwtDevise.maximum_active_devices > 0 and user.refresh_tokens.active.count >= JwtDevise.maximum_active_devices
          return render status: :unauthorized, json: {error: I18n.t('errors.maximum_devices')}
        elsif user.valid_password? password
          refresh_token = user.create_my_refresh_token
          return render status: :ok, json: {access_token: user.encode_my_jwt, refresh_token: refresh_token} unless refresh_token.nil?
        end
      end
      render status: :unauthorized, json: {error: I18n.t('errors.invalid_credentials')}
    else
      render status: :unprocessable_entity, json: {error: I18n.t('errors.invalid_format')}
    end
  end

  private

  def has_valid_params?
    return false if credentials.empty? || user_params[:password].nil?
    true
  end

  def credentials
    (JwtDevise.possible_credentials & user_params.keys.map(&:to_sym))
  end

  def key
    credentials.first
  end

  def user
    JwtDevise.user_class.where(scope).find_by(key => user_params[key])
  end

  def unscoped_user
    JwtDevise.user_class.find_by(key => user_params[key])
  end

  def wrong_scope
    unscoped_user.present? && user.nil?
  end

  def scope
    JwtDevise.user_scope
  end

  def scope_error
    JwtDevise.scope_error
  end

  def notify_scope_error
    JwtDevise.notify_scope_error
  end

  def password
    user_params[:password]
  end

  def user_params
    params[JwtDevise.user_class.model_name.param_key]
  end
end
