class RefreshToken < ApplicationRecord
  belongs_to :user, class_name: JwtDevise.user_class.to_s

  scope :active, -> { where('expires_at > ?', Time.now) }
end
