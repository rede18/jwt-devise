require "jwt_devise/engine"

module JwtDevise
  # Your code goes here...
  mattr_accessor :user_class
  mattr_accessor :expiration
  mattr_accessor :issuer
  mattr_accessor :audience
  mattr_accessor :secret
  mattr_accessor :algorithm
  mattr_accessor :leeway
  mattr_accessor :refresh_expiration
  mattr_accessor :possible_credentials
  mattr_accessor :maximum_active_devices
  mattr_accessor :user_scope
  mattr_accessor :scope_error
  mattr_accessor :notify_scope_error

  # To save having to call constantize on the user_class result all the time, you could instead just override the
  # user_class getter method inside the JwtDevise module to always call constantize on the
  # saved value before returning the result:
  def self.user_class
    @@user_class.constantize
  end
end
