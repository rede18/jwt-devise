require 'securerandom'
require 'bcrypt'

module Devise
  module Models
    module JwtAuthenticatable
      extend ActiveSupport::Concern

      included do
        has_many :refresh_tokens
      end

      def encode_my_jwt
        JwtDevise::Wrapper.encode(self.id)
      end

      # Return token if true, nil otherwise
      def create_my_refresh_token
        token = SecureRandom.hex(25)
        record = self.refresh_tokens.create(token: BCrypt::Password.create(token), expires_at: Time.now + JwtDevise.refresh_expiration)
        return "#{token}.#{record.id}" if record
        nil
      end

    end
  end
end