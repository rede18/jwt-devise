require 'devise'

module Devise
  module Strategies
    class JsonWebToken < Base
      def valid?
        request.headers['Authorization'].present?
      end

      def authenticate!
        return fail! unless claims
        return fail! unless claims.has_key?('sub')

        success! mapping.to.find_by_id claims['sub']
      end

      def store?
        false
      end

      protected ######################## PROTECTED #############################

      def claims
        JwtDevise::Wrapper.claims(request.headers)
      end
    end
  end
end
