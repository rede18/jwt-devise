module JwtDevise
  module Wrapper
    extend self

    def encode(uid, expiration = nil)
      now_seconds = Time.now.to_i

      expiration ||= JwtDevise.expiration.to_i

      raise 'You must set up JWT_EXPIRATION in your config/secrets.yml' if JwtDevise.expiration.nil?

      payload = {
          iss: JwtDevise.issuer,
          sub: uid,
          aud: JwtDevise.audience,
          iat: now_seconds,
          exp: now_seconds + expiration
      }

      raise 'You must set up JWT_SECRET in your config/secrets.yml' if JwtDevise.secret.nil?

      JWT.encode payload, openssl_secret, JwtDevise.algorithm
    end

    def decode(token, options = {})
      decoded_token = JWT.decode token, openssl_secret, true, options

      decoded_token.first
    end

    def claims(headers, options = {})
      strategy, token = split(headers)

      return nil if (strategy || '').downcase != 'bearer'

      JwtDevise::Wrapper.decode(token, options) rescue nil
    end

    def split(headers)
      headers['Authorization'].split(' ')
    end

    private

    def openssl_secret
      retrieve_openssl_secret!
    rescue OpenSSL::PKey::RSAError
      retrieve_openssl_secret
    end

    def retrieve_openssl_secret!
      OpenSSL::PKey::RSA.new(JwtDevise.secret)
    end

    def retrieve_openssl_secret
      OpenSSL::PKey::RSA.new(JwtDevise.secret.gsub(/\\n/, "\n"))
    end

  end
end
