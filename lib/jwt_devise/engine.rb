require 'devise/models/jwt_authenticatable'
require 'jwt_devise/wrapper'
require 'devise/strategies/json_web_token'
require 'devise'
require 'jwt'

module JwtDevise
  class Engine < ::Rails::Engine

  end
end
