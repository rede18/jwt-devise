require 'rails_helper'
require 'timecop'
require 'byebug'

RSpec.describe "JWT", type: :request do
  describe "JWT management" do

    before do
      @user = User.create(email: 'iandos@gmail.com', password: 'secret', cpf: '01102203344')
    end

    context "when trying to login in" do
      it "retrieves JWT from existing user with email" do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
        expect(response).to have_http_status(:ok)
      end

      it "fails to retrieve JWT from existing user with incorred password" do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'not_secret'}}
        expect(response).to have_http_status(:unauthorized)
      end

      it "fails to retrieve JWT from existing user with incorred password in pt_BR" do
        I18n.locale = 'pt_BR'
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'not_secret'}}
        json = JSON.parse(response.body)
        expect(response).to have_http_status(:unauthorized)
        expect(json['error']).to eq(I18n.t('errors.invalid_credentials'))
        I18n.locale = :en
      end

      it "fails to retrieve JWT from existing user with incorred credential keys" do
        post '/jwt/sign_in.json', params: {user: {address: @user.email, password: 'secret'}}
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "retrieves JWT from existing user with cpf" do
        post '/jwt/sign_in.json', params: {user: {cpf: @user.cpf, password: 'secret'}}
        expect(response).to have_http_status(:ok)
      end

      it "mades a bad request for JWT from existing user" do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, senha: 'secret'}}
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "logs in with scope defined" do
        JwtDevise.user_scope = {status: :nice}
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
        JwtDevise.user_scope = {}
        expect(response).to have_http_status(:ok)
      end

      it "fails to log in with scope defined and notify scope error" do
        JwtDevise.user_scope = {status: :boring}
        JwtDevise.notify_scope_error = true
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
        JwtDevise.notify_scope_error = false
        JwtDevise.user_scope = {}
        expect(response).to have_http_status(:forbidden)
      end

      it "fails to log in with scope defined and if don't notify scope error" do
        JwtDevise.user_scope = {status: :boring}
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
        JwtDevise.user_scope = {}
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "when logged in" do

      before do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
        json = JSON.parse(response.body)
        @access_token = json['access_token']
        @refresh_token = json['refresh_token']
      end

      it "refreshes JWT for user after 15 minutes" do
        Timecop.freeze(Time.now + 15.minutes) do
          post '/jwt/refresh.json', params: {refresh_token: {token: @refresh_token}}, headers: {Authorization: "Bearer #{@access_token}"}
          expect(response).to have_http_status(:ok)
        end
      end

      it "does not refreshes JWT for user before 15 minutes" do
        Timecop.freeze(Time.now + 5.minutes) do
          post '/jwt/refresh.json', params: {refresh_token: {token: @refresh_token}}, headers: {Authorization: "Bearer #{@access_token}"}
          json = JSON.parse(response.body)
          expect(json['access_token']).to eql(@access_token)
        end
      end

      it "does not refresh JWT for user after 2 weeks" do
        # Two hours ahead since Timecop handles timezone incorrectly with GMT-2 instead of GMT-3.
        Timecop.freeze(Time.now + JwtDevise.leeway + 2.hours) do
          post '/jwt/refresh.json', params: {refresh_token: {token: @refresh_token}}, headers: {Authorization: "Bearer #{@access_token}"}
          expect(response).to have_http_status(:unauthorized)
        end
      end

      it "does not refresh JWT if refresh token has been tampered" do
        Timecop.freeze(Time.now + 15.minutes) do
          post '/jwt/refresh.json', params: {refresh_token: {token: @refresh_token + 'aisjhgajgsgfa'}}, headers: {Authorization: "Bearer #{@access_token}"}
          expect(response).to have_http_status(:unauthorized)
        end
      end

      describe "signing out" do

        before(:each) do
          post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
          json = JSON.parse(response.body)
          @access_token = json['access_token']
          @refresh_token = json['refresh_token']
        end

        it "can sign out expiring a refresh token" do
          post '/jwt/sign_out.json', params: {refresh_token: {token: @refresh_token}}, headers: {Authorization: "Bearer #{@access_token}"}
          expect(response).to have_http_status(:ok)
        end

        it "can not sign out with a expired access token" do
          Timecop.freeze(Time.now + JwtDevise.leeway + 2.hours) do
            post '/jwt/sign_out.json', params: {refresh_token: {token: @refresh_token}}, headers: {Authorization: "Bearer #{@access_token}"}
            expect(response).to have_http_status(:unauthorized)
          end
        end
      end
    end

    it "cannot create more than maximum active refresh_tokens" do
      JwtDevise.maximum_active_devices.times do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
      end
      post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
      expect(response).to have_http_status(:unauthorized)
    end

    it "can create unlimited active refresh tokens" do
      JwtDevise.maximum_active_devices = 0
      10.times do
        post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
      end
      post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
      JwtDevise.maximum_active_devices = 10
      expect(response).to have_http_status(:ok)
    end

    it "can retrieve JWT SECRET safely from environment such as Elastic Beanstalk" do
      backup_secret = JwtDevise.secret
      JwtDevise.secret = JwtDevise.secret.gsub(/\n/, "\\n")
      post '/jwt/sign_in.json', params: {user: {email: @user.email, password: 'secret'}}
      JwtDevise.secret = backup_secret
      expect(response).to have_http_status(:ok)
    end

  end
end
