class CreateRefreshTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :refresh_tokens do |t|
      t.string :token, null: false, default: ""
      t.references :user
      t.datetime :expires_at
      t.string :device_name, null: false, default: ""
      t.timestamps null: false
    end

    add_index :refresh_tokens, :token, unique: true
  end
end
