$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "jwt_devise/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "jwt_devise"
  s.version     = JwtDevise::VERSION
  s.authors     = ["Ian Ferreira dos Santos"]
  s.email       = ["iandos@gmail.com"]
  s.homepage    = "https://www.yimobile.com.br"
  s.summary     = "Summary of JwtDevise."
  s.description = "Description of JwtDevise."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 5.1.4"
  s.add_dependency "jwt", "~> 1.5.6"
  s.add_dependency "devise", "~> 4.4.3"

  s.add_development_dependency "mysql2"
  s.add_development_dependency "devise", "~> 4.4.3"
  s.add_development_dependency "byebug"
  s.add_development_dependency "rspec-rails", "~> 3.6"
  s.add_development_dependency "timecop"
end
